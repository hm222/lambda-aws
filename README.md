# AWS Lambda Function with REST API

This repository contains the source code for an AWS Lambda function accessible via an AWS REST API, deployed using Amazon API Gateway. This setup allows the Lambda function to be invoked through HTTP requests, enabling integration with web applications, mobile apps, and other cloud services.

## Features

- Serverless execution of code in response to HTTP requests.
- Integration with AWS services and external APIs.

## Prerequisites

- [Cargo Lambda](https://www.cargo-lambda.info/)

## Setup and Deployment

The Lambda function and REST API are already deployed on AWS.

## Usage

Invoke the Lambda function via the REST API using HTTP methods (e.g., GET, POST). Here’s a generic example using `curl`:

```bash
curl https://3rep2iouu6.execute-api.us-east-2.amazonaws.com/test/LambdaResource -d '{"httpMethod": "POST", "path": "/my/path", "body": "{\"name\":\"HM\"}", "headers": {"Content-Type": "application/json"}}'
```

Replace the "name" field with any names you would like to use, and you will see the change in the message of the response.

## Testing

- Use Postman or a similar tool to send requests to your API endpoint.
- Verify the Lambda function executes correctly and the expected response is returned.

## Acknowledgments

- AWS documentation and tutorials.
- Open-source projects and libraries used in this function.